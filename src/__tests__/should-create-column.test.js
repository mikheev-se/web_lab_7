import React from 'react';
import { fireEvent, waitFor, render } from '@testing-library/react';
import { it, expect } from '@jest/globals';
import App from '../App';

jest.mock('../api', () => {
  return {
    api: {
      getCurrentUser: async () => {
        return { name: 'John Doe' };
      },
      getColumns: async () => {
        return [];
      },
      createColumn: async (columnName) => {
        return { id: Date.now(), name: columnName };
      },
    },
  };
});

it(`App должен содержать форму для добавления колонки.
По клику должна добавляться колонка.`, async () => {
  const NEW_COLUMN_NAME = 'New column';

  const { container, findByText } = render(<App />);

  const columnNameInput = container.querySelector('#create_column_input');
  const createColumnButton = container.querySelector('#create_column_button');

  fireEvent.change(columnNameInput, { target: { value: NEW_COLUMN_NAME } });
  fireEvent.click(createColumnButton);

  await waitFor(
    async () => {
      const newColumn = await findByText(NEW_COLUMN_NAME);
      expect(newColumn).toBeTruthy();
    },
    { timeout: 2000 }
  );
});
